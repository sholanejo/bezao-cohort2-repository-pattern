﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace BEZAO_PayDAL.Model
{
   public class RegisterViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public DateTime Birthday { get; set; }

        private static string getHASH(string text)
        {
            using(var sha256 = SHA256.Create())
            {
                var hashedbytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(text));
                return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
            }
        }
    }
}
