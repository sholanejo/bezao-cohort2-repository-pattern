﻿using System;
using System.Collections.Generic;
using System.Linq;
using BEZAO_PayDAL.Entities;
using BEZAO_PayDAL.Interfaces.Repositories;
using BEZAO_PayDAL.Interfaces.Services;
using BEZAO_PayDAL.Model;
using BEZAO_PayDAL.Repositories;
using BEZAO_PayDAL.Services;
using BEZAO_PayDAL.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace CodeFirstSoln
{
    partial class Program
    {
        static void Main(string[] args)
        {
            //EnrollUser();
            //DeleteUser(10);
            //GetUser(10);
            //UpdateUser("test.t@tester.com", "Testor", "nwpassword", "Nwpassword", "Nwpassword");
            //Login("test.t@tester.com", "nwpassword");

        }

        static void EnrollUser()
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Register(new RegisterViewModel
            {
                FirstName = "Test", 
                LastName = "Testing", 
                Email = "test.t@tester.com", 
                Username = "Testor",
                Birthday = new DateTime(1999, 05, 12),
                Password = "nwpassword",
                ConfirmPassword = "nwpassword"
            });
        }
        static void DeleteUser(int Id)
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Delete(Id);
        }
        static void GetUser(int Id)
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Get(Id);
        }
        static void UpdateUser(string email, string username, string currentPassword, string newPassword, string confirmNewPassword)
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Update(new UpdateViewModel {Email=email, Username = username, CurrentPassword = currentPassword,
                NewPassword = newPassword, ConfirmNewPassword =confirmNewPassword  });
        }
        static void Login(string emailUsername, string password)
        {
            IUserService userService = new UserService(new UnitOfWork(new BezaoPayContext()));
            userService.Login(new LoginViewModel {UsernameEmail = emailUsername, Password = password });
        }


    }
}
